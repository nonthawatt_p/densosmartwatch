package com.ssd.densosmartwatch

import android.app.ListFragment
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.*
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.ParcelUuid
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import com.ssd.densosmartwatch.adapter.ScanResultAdapter
import com.ssd.densosmartwatch.utilities.KUtil
import kilmuz.kotlinlib.lib.KAlert
import java.util.*

class WearableFragment : ListFragment() {

    private var mBluetoothAdapter: BluetoothAdapter? = null
    private var mBluetoothLeScanner: BluetoothLeScanner? = null

    private var mAdapter: ScanResultAdapter? = null

    private var mCallback: ScanCallback? = null

    private var mHandler: Handler? = null

    private val mRunnable = Runnable { stop() }

    /**
     * Must be called after object creation by MainActivity.
     *
     * @param bluetoothAdapter the local BluetoothAdapter
     */
    fun setBluetoothAdapter(bluetoothAdapter: BluetoothAdapter?) {
        this.mBluetoothAdapter = bluetoothAdapter

        mBluetoothLeScanner = mBluetoothAdapter?.bluetoothLeScanner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        retainInstance = true

        // Use getActivity().getApplicationContext() instead of just getActivity() because this
        // object lives in a fragment and needs to be kept separate from the Activity lifecycle.
        //
        // We could get a LayoutInflater from the ApplicationContext but it messes with the
        // default theme, so generate it from getActivity() and pass it in separately.
        mAdapter = ScanResultAdapter(activity.applicationContext, LayoutInflater.from(activity))

        mHandler = Handler()

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        listAdapter = mAdapter

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listView.divider = null
        listView.dividerHeight = 0

        setEmptyText("No devices found - refresh to try again.")

        // Trigger refresh on app's 1st load
        start()
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        //super.onListItemClick(l, v, position, id);

        val device = mAdapter?.getItem(position) ?: return

        KAlert.show(activity,getString(R.string.confirm),"Do you want to connect this device (${device.address})",getString(R.string.cancel),getString(R.string.ok), {
            onOK {
                mHandler?.removeCallbacks(mRunnable)
                mBluetoothLeScanner?.stopScan(mCallback)
                l!!.invalidateViews()

                /** Set Mac Address to prefs. */
                KUtil.setMacAddress(activity,device.address)

                if((activity as WearableActivity).isSplash) {
                    val intent = Intent(activity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    activity.overridePendingTransition(R.anim.push_in, R.anim.push_in_exit)
                } else {
                    activity.onBackPressed()
                }
            }
            onCancel {
                it.dismiss()
            }
        })

        //Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
        //intent.putExtra(DeviceActivity.EXTRAS_DEVICE_NAME, device.getName());
        //intent.putExtra(DeviceActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());

        //startActivity(intent);
    }

    /**
     * Start scanning for BLE Advertisements (& set it up to stop after a set period of time).
     */
    fun start() {
        if (mCallback == null) {

            // Kick off a new scan.
            mCallback = CustomScanCallback()
            mBluetoothLeScanner?.startScan(buildScanFilters(), buildScanSettings(), mCallback)

//            Toast.makeText(activity, "Scanning for " + TimeUnit.SECONDS.convert(SCAN_PERIOD, TimeUnit.MILLISECONDS) + " seconds.", Toast.LENGTH_LONG).show()
            Toast.makeText(activity,"Scanning...",Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(activity, "Scanning already started.", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Stop scanning for BLE Advertisements.
     */
    fun stop() {
        // Stop the scan, wipe the callback.
        mBluetoothLeScanner?.stopScan(mCallback)
        mCallback = null

        // Even if no new results, update 'last seen' times.
        mAdapter?.notifyDataSetChanged()
        mAdapter?.clear()


    }

    /**
     * Return a List of [ScanFilter] objects to filter by Service UUID.
     */
    private fun buildScanFilters(): List<ScanFilter> {
        val scanFilters = ArrayList<ScanFilter>()

        val builder = ScanFilter.Builder()
        // Comment out the below line to see all BLE devices around you
        //builder.setServiceUuid(SERVICE_UUID);

        scanFilters.add(builder.build())

        return scanFilters
    }

    /**
     * Return a [ScanSettings] object set to use low power (to preserve battery life).
     */
    private fun buildScanSettings(): ScanSettings {
        val builder = ScanSettings.Builder()
        builder.setScanMode(ScanSettings.SCAN_MODE_BALANCED)

        return builder.build()
    }

    /**
     * Custom ScanCallback object - adds to adapter on success, displays error on failure.
     */
    private inner class CustomScanCallback : ScanCallback() {
        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)

            for (result in results) {
                if (result.device.type == BluetoothDevice.DEVICE_TYPE_LE) {
                    mAdapter?.add(result)
                }
            }

            mAdapter?.notifyDataSetChanged()
        }

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)

            if (result.device.type == BluetoothDevice.DEVICE_TYPE_LE) {
                mAdapter?.add(result)
            }

            mAdapter?.notifyDataSetChanged()
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)

//            Toast.makeText(activity, "Scan failed with error: " + errorCode, Toast.LENGTH_LONG).show()
        }
    }

    override fun onStop() {
        stop()
        super.onStop()
    }

    companion object {
        /**
         * UUID identified with this app - set as Service UUID for BLE Advertisements.
         *
         *
         * Bluetooth requires a certain format for UUIDs associated with Services.
         * The official specification can be found here:
         * [://www.bluetooth.org/en-us/specification/assigned-numbers/service-discovery][https]
         */
        val SERVICE_UUID = ParcelUuid.fromString("0000b81d-0000-1000-8000-00805f9b34fb")
        private val SCAN_PERIOD: Long = 10000
    }
}


