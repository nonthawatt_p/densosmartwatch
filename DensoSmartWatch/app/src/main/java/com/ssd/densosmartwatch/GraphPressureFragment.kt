package com.ssd.densosmartwatch


import android.app.Fragment
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Button
import android.widget.FrameLayout
import com.ssd.densosmartwatch.utilities.KUtil
import kilmuz.kotlinlib.lib.printlog
import okhttp3.OkHttpClient
import okhttp3.Request
import java.net.URI


class GraphPressureFragment : Fragment() {

    companion object {
        fun newInstance() = GraphPressureFragment()
    }

    private val kTitle = "Blood Pressure"
    private val URLPath = "http://192.168.1.85:8085/bloodpressure"
    lateinit var webView :WebView
    lateinit var coverFrame: FrameLayout
    lateinit var btnRefresh: Button
    var loadFinished = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_graph_pressure, container, false)

        webView = view!!.findViewById(R.id.pressure_webview)
        coverFrame = view!!.findViewById(R.id.cover_pressure)
        btnRefresh = view!!.findViewById(R.id.btn_pressure_refresh)

        btnRefresh.setOnClickListener {
            loadURL()
        }

        coverFrame.visibility = View.GONE
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                loadFinished = true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                val redirectURI = URI(url)
                val originalURI = URI(URLPath)
                printlog.d("redirect :: $redirectURI")
                printlog.d("redirect :: $originalURI")
                loadFinished = originalURI.host == redirectURI.host

                return super.shouldOverrideUrlLoading(view, url)
            }

            @RequiresApi(api = Build.VERSION_CODES.M)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val redirectURI = URI(request?.url.toString())
                val originalURI = URI(URLPath)
                printlog.d("redirect :: $redirectURI")
                printlog.d("redirect :: $originalURI")

                loadFinished = originalURI.host == redirectURI.host
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                // todo test
                printlog.e("Error !!!")
                loadFinished = false
                coverFrame.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                printlog.d("onPageFinished :: $loadFinished")
                if(loadFinished) {
                    coverFrame.visibility = View.GONE
                } else {
                    coverFrame.visibility = View.VISIBLE
                }
            }

            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {

                val url = request.url.toString()
                return getNewResponse(url)
            }

            private fun getNewResponse(url: String): WebResourceResponse? {
                try {
                    val httpClient = OkHttpClient()
                    val request = Request.Builder().url(url)

                    KUtil.acToken?.let {
                        if (it.getAuth() != null) {
                            request.addHeader("Authorization", it.getAuth()!!)
                        }
                    }
                    val response = httpClient.newCall(request.build()).execute()
                    return WebResourceResponse(
                            null,
                            response.header("content-encoding", "utf-8"),
                            response.body()?.byteStream()
                    )

                } catch (e: Exception) {
                    printlog.e("request error Webview : ${e.localizedMessage}")
                    return null
                }

            }
        }

        loadURL()
        return view
    }

    fun loadURL() {
//        KUtil.acToken?.let {
//            webView.loadUrl(URLPath, mapOf("Authorization" to it.getAuth()))
//            printlog.d("Load Header =>> : ${it.getAuth()}")
//        } ?: run {
            webView.loadUrl(URLPath)
//        }
    }

}// Required empty public constructor
