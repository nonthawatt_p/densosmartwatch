package kilmuz.kotlinlib.lib

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.ssd.densosmartwatch.R
import com.ssd.densosmartwatch.utilities.KUtil
import java.util.regex.Matcher
import java.util.regex.Pattern


class Extension {}

/// EditText
fun EditText.isEmailValid(): Boolean {
    val email = this.text
    val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
    val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(email)
    return matcher.matches()
}

fun EditText.isPasswordValid():Boolean {
    val pattern: Pattern
    val matcher: Matcher
    val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=.-_!?;:'\"*`~|%()\\[\\]<>×÷\\{\\}©®£€¥^])(?=\\S+$).{8,}$"
    pattern = Pattern.compile(PASSWORD_PATTERN)
    matcher = pattern.matcher(this.text)
    return matcher.matches()
}

/**
 * Check network is connected to internet.
 */
fun Activity.isNetworkConnected(): Boolean  = (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo != null

fun Activity.showNoInternet() {
    KAlert.showWithOK(this,R.string.no_internet_title,R.string.no_internet_msg,null)
}

fun Activity.showNotConnectServer() {
    KAlert.showWithOK(this,R.string.not_connect_server,R.string.not_connect_server_msg,null)
}

/**
 * Check network connection and check network condition of application.
 * @return if not a network will show alert dialog.
 */
fun Activity.checkInternet(): Boolean {
    val isNetwork = isNetworkConnected()
    if(!isNetwork) {
        showNoInternet()
    } else {
        val isConnection = checkCanSendData()
        if(!isConnection) {
            showNotConnectServer()
        }
        return isConnection
    }
    return isNetwork
}

/**
 * Check network and connection type from settings of this app.
 * This function is not show alert dialog if not network connection.
 * @return TRUE, if pass all condition. FALSE, if not match condition.
 */
fun Activity.checkCanSendData(): Boolean = if(isNetworkConnected()) {
    if( KUtil.networkCellular ) {
        KUtil.is3G(this) || KUtil.isWiFi(this)
    } else {
        KUtil.isWiFi(this)
    }
} else {
    // Not connect to internet
    false
}

fun Activity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Activity.goActivity(clazz: Class<*>) {
    val intent = Intent(this, clazz)
    startActivity(intent)
    overridePendingTransition(R.anim.push_in,R.anim.push_in_exit)
}

object printlog {
    private val TAG = "!-DENSO-!"
    fun d(text: String?) {
        Log.d(TAG,text)
    }
    fun e(text:String?) {
        Log.e(TAG,text)
    }
    fun i(text:String?) {
        Log.i(TAG,text)
    }
    fun v(text:String?) {
        Log.v(TAG,text)
    }
    fun obj(ob:Any?) {
        if(ob==null) return
        KUtil.printJSONFrom(ob)
    }
}



