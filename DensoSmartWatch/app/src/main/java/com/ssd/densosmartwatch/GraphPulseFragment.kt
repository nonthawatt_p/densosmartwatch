package com.ssd.densosmartwatch

import android.annotation.TargetApi
import android.app.Fragment
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Button
import android.widget.FrameLayout
import com.ssd.densosmartwatch.utilities.KUtil
import kilmuz.kotlinlib.lib.printlog
import okhttp3.OkHttpClient
import okhttp3.Request
import java.net.URI


class GraphPulseFragment : Fragment() {



    companion object {
        fun newInstance() = GraphPulseFragment()
    }


    val URLPath = "http://192.168.1.85:8085/pulserate"
    lateinit var webView: WebView
    lateinit var coverFrame: FrameLayout
    lateinit var btnRefresh: Button
    var loadFinished = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_graph_pulse, container, false)

        webView = view!!.findViewById(R.id.pulserate_webview)
        coverFrame = view!!.findViewById(R.id.cover_pulse)
        btnRefresh = view!!.findViewById(R.id.btn_pulse_refresh)

        btnRefresh.setOnClickListener {
            loadURL()
        }

        coverFrame.visibility = View.GONE
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                loadFinished = true
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                val redirectURI = URI(url)
                val originalURI = URI(URLPath)
                printlog.d("redirect :: $redirectURI")
                printlog.d("redirect :: $originalURI")
                loadFinished = originalURI.host == redirectURI.host

                return true
            }

            @TargetApi(Build.VERSION_CODES.M)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val redirectURI = URI(request?.url.toString())
                val originalURI = URI(URLPath)
                printlog.d("redirect :: $redirectURI")
                printlog.d("redirect :: $originalURI")

                loadFinished = originalURI.host == redirectURI.host
                return true
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                // todo test
                printlog.e("Error !!!")
                loadFinished = false
                coverFrame.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                printlog.d("onPageFinished :: $loadFinished")
                if(loadFinished) {
                    coverFrame.visibility = View.GONE
                } else {
                    coverFrame.visibility = View.VISIBLE
                }
            }

            override fun onLoadResource(view: WebView?, url: String?) {

            }

            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                val url = request.url.toString()
                return getNewResponse(url, request)
            }

            private fun getNewResponse(url: String, data:WebResourceRequest): WebResourceResponse? {
                try {

                    val request = Request.Builder().url(url)
//                    KUtil.printJSONFrom(data.url.queryParameterNames)

                    for( maps in data.requestHeaders )
                        request.addHeader(maps.key ,maps.value)

                    KUtil.acToken?.let {
                        if (it.getAuth() != null) {
                            printlog.d("auth => ${it.getAuth()}")
                            request.addHeader("Authorization", it.getAuth()!!)
                        }
                    }

                    KUtil.printJSONFrom(request)
                    val response = OkHttpClient().newCall(request.build()).execute()
                    return WebResourceResponse(
                            null,
                            response.header("content-encoding", "utf-8"),
                            response.body()!!.byteStream()
                    )

                } catch (e: Exception) {
                    printlog.e("request error Webview : ${e.localizedMessage}")
                    return null
                }

            }
        }

        loadURL()
        return view
    }

    fun loadURL() {
//        KUtil.acToken?.let {
//            val headers = hashMapOf("Authorization" to it.getAuth())
//            webView.loadUrl(URLPath , headers)
//        } ?: run {
            webView.loadUrl(URLPath)
//        }
    }

}
