package com.ssd.densosmartwatch

import android.app.Activity
import android.app.AlertDialog
import android.app.Fragment
import android.content.Context
import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.ssd.densosmartwatch.utilities.KUtil
import kilmuz.kotlinlib.lib.goActivity


class SettingsFragment : Fragment() {

    companion object {
        fun newInstance() = SettingsFragment()
    }

    private lateinit var listView: ListView
    private lateinit var settingsAdapter:SettingsAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater!!.inflate(R.layout.fragment_settings, container, false)
        activity.requestedOrientation = SCREEN_ORIENTATION_PORTRAIT
        activity.title = getString(R.string.menu_settings)

        listView = view.findViewById(R.id.lv_settings)


        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        settingsAdapter = SettingsAdapter(activity)

        listView.adapter = settingsAdapter
        listView.setOnItemClickListener { parent, view, position, id ->
            when(position) {
                0->{
                    var isLocation = true
                    AlertDialog.Builder(activity)
                            .setTitle(R.string.enable_location_data)
                            .setSingleChoiceItems(arrayOf("ON","OFF"),if(KUtil.locationData) 0 else 1,{ dialog, which ->
                                isLocation = (which==0)
                            })
                            .setPositiveButton(R.string.ok,{ dialog, which ->
                                KUtil.locationData = isLocation
                                settingsAdapter.notifyDataSetChanged()
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .setCancelable(false)
                            .create().show()
                }
                1->{
                    var isCellular = true
                    AlertDialog.Builder(activity)
                            .setTitle(R.string.choose_network_service)
                            .setSingleChoiceItems(arrayOf(getString(R.string.wifi),getString(R.string.wifi_cellular)),if(KUtil.networkCellular) 1 else 0,{ dialog, which ->
                                isCellular = (which==1)
                            })
                            .setPositiveButton(R.string.ok,{ dialog, which ->
                                KUtil.networkCellular = isCellular
                                settingsAdapter.notifyDataSetChanged()
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .setCancelable(false)
                            .create().show()
                }
                2->{
                    activity.goActivity(WearableActivity::class.java)
                }
                else->{}
            }
        }

    }

    override fun onResume() {
        super.onResume()
        settingsAdapter.notifyDataSetChanged()
    }


    inner class SettingsAdapter(
            private var context: Context) : BaseAdapter() {
        override fun getItem(p0: Int): Any = when(p0) {
            0->{ if(KUtil.locationData) getString(R.string.on) else getString(R.string.off) }
            1->{ if(KUtil.networkCellular) getString(R.string.wifi_cellular) else getString(R.string.wifi) }
            2->{ KUtil.mcAddress ?: "" }
            else-> ""
        }

        override fun getItemId(p0: Int): Long = p0.toLong()

        override fun getCount(): Int = 3


        override fun getView(position: Int, view: View?, parent: ViewGroup): View {
            var view = view
            // Reuse an old view if we can, otherwise create a new one.
            if (view == null) {
                val inflater = (context as Activity).layoutInflater
                view = inflater.inflate(R.layout.adapter_settings, null)
            }
            val tvTitle = view!!.findViewById<TextView>(R.id.tv_settings_title)
            val tvDetail = view!!.findViewById<TextView>(R.id.tv_settings_detail)
            val imvIcon = view!!.findViewById<ImageView>(R.id.imv_settings)

            skip@ when (position) {
                0 -> {// Location
                    imvIcon.setImageResource(R.drawable.ic_location)
                    tvTitle.text = getString(R.string.location_data)
                    val location = if(KUtil.locationData) getString(R.string.on) else getString(R.string.off)
                    tvDetail.text = location
                }
                1 -> {
                    imvIcon.setImageResource(R.drawable.ic_network)
                    tvTitle.text = getString(R.string.network_service)
                    val network = if(KUtil.networkCellular) getString(R.string.wifi_cellular) else getString(R.string.wifi)
                    tvDetail.text = network
                }
                2 -> {
                    imvIcon.setImageResource(R.drawable.ic_watch)
                    tvTitle.text = getString(R.string.wearable)
                    val wearable = KUtil.mcAddress ?: return@skip
                    tvDetail.text = wearable
                }
                else -> {
                    //skip
                }
            }

            return view!!
        }

        inner class ViewHolder (
            var title:String?,
            var detail:String?
        )

    }

}