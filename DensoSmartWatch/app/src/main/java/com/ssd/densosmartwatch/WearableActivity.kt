package com.ssd.densosmartwatch

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_wearable.*

class WearableActivity : KActivity() {

    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null

    private var wearableFrag = WearableFragment()
    private var thread:Thread? = null

    var isSplash = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wearable)
        isSplash = intent.getBooleanExtra("fromSplash",false)
        setToolbar(getString(R.string.wearable),!isSplash)

        Handler().postDelayed({
            mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            mBluetoothAdapter = mBluetoothManager?.adapter
            wearableFrag.setBluetoothAdapter(mBluetoothAdapter)
        }, 10)
//        thread = Thread({
//            mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
//            mBluetoothAdapter = mBluetoothManager?.adapter
//            wearableFrag.setBluetoothAdapter(mBluetoothAdapter)
//        })
//        thread?.start()

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.wearable_layout, wearableFrag)
        transaction.commit()
        System.gc()

        swiperefresh.setOnRefreshListener {
            wearableFrag.stop()
            mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            mBluetoothAdapter = mBluetoothManager?.adapter
            wearableFrag.setBluetoothAdapter(mBluetoothAdapter)
            wearableFrag.start()

            swiperefresh.isRefreshing = false
        }

    }

    override fun onStop() {
        System.gc()
        thread?.interrupt()
        super.onStop()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.pop_out,R.anim.pop_out_exit)
    }

}
