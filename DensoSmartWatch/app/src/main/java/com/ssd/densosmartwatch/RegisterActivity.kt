package com.ssd.densosmartwatch

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.ssd.densosmartwatch.services.DService
import com.ssd.densosmartwatch.services.ErrorResponse
import com.ssd.densosmartwatch.services.RegisterRequest
import com.ssd.densosmartwatch.services.TokenResponse
import com.ssd.densosmartwatch.utilities.KUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kilmuz.kotlinlib.lib.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.HttpException

class RegisterActivity : KActivity() {

    var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setToolbar(getString(R.string.register),true)

        edtregis_wearable.setText(KUtil.mcAddress)

        // TODO DEBUG... delete when release...
//        edtregis_passw.setText("Welcome@1")
//        edtregis_re_passw.setText("Welcome@1")

        btnregis_submit.setOnClickListener {
            hideKeyboard()
            /** Check field is empty. */
            if(edtregis_email.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@RegisterActivity, R.string.warning, R.string.alert_pls_input_email, null)
                return@setOnClickListener
            }
            if(edtregis_passw.text.isNullOrEmpty() && edtregis_re_passw.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@RegisterActivity,R.string.warning,R.string.alert_pls_input_passw_and_re_passw, null)
                return@setOnClickListener
            }
            if(edtregis_passw.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@RegisterActivity, R.string.warning, R.string.alert_pls_input_passw, null)
                return@setOnClickListener
            }
            if(edtregis_re_passw.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@RegisterActivity, R.string.warning, R.string.alert_pls_input_re_passw, null)
                return@setOnClickListener
            }

            /** If field is not empty. Check format and password. */
            if(!edtregis_email.isEmailValid()) {
                KAlert.showWithOK(this@RegisterActivity,R.string.warning,R.string.alert_email_invalid, {})
                return@setOnClickListener
            }
            if(edtregis_passw.text.length<8) {
                KAlert.showWithOK(this@RegisterActivity,R.string.warning,R.string.alert_passw_should_min_8_digit, {})
                return@setOnClickListener
            }
            if(edtregis_passw.text.toString() != edtregis_re_passw.text.toString()) {
                KAlert.showWithOK(this@RegisterActivity,R.string.warning,R.string.alert_passw_not_match, {})
                return@setOnClickListener
            }

            /** Check internet. */
            if(!checkInternet()) { return@setOnClickListener }

            /** If field is pass all condition. Show progress dialog and call service. */
            progressDialog = ProgressDialog.show(this,null,getString(R.string.loading), true)
            callRegister(edtregis_email.text.toString(), edtregis_passw.text.toString())
        }

    }

    private fun callRegister(email:String, passw:String) {
        DService.call.register(RegisterRequest(email,passw,passw))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext {}
                .doOnTerminate { progressDialog?.dismiss() }
//                    .doOnComplete(completeAction)
                .subscribe ({
                    // Register success!!
                    KAlert.showWithOK(this@RegisterActivity, R.string.success, R.string.alert_regis_success, {
                        progressDialog = ProgressDialog.show(this,null,getString(R.string.loading), true)
                        login()
                    })

                }, { error ->
                    printlog.d("SERVICE: error : =>> ${error.message}")
                    if (error is HttpException) {
                        // We had non-2XX http error
                        if (error.response().errorBody() != null) {
                            val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(), ErrorResponse::class.java)
                            KAlert.show(this,getString(R.string.error),"${erResponse.message}", {})
                        } else {
                            /// error body is null
                        }
                    } else {
                        /// error is not HttpException
                    }
                })
    }

    private fun login() {
        DService.call.token(edtregis_email.text.toString(),edtregis_passw.text.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext { token ->
                    // Call service 'UserInfo' to get user data.
                    KUtil.printJSONFrom(token)
                    callUserInfo(token)
                }
                .doOnError {
                    progressDialog?.dismiss()
                }
                .subscribe (Consumer {},errorAction)
    }

    private fun callUserInfo(tokens: TokenResponse) {
        KUtil.acToken = tokens
        KUtil.setToken(this, tokens)
        DService.call.userInfo(tokens.getAuth()!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnTerminate { progressDialog?.dismiss() }
                .doOnNext { user -> KUtil.userInfo = user }
                .doOnComplete(completeAction)
                .subscribe(Consumer { user -> KUtil.printJSONFrom(user) },errorAction)
    }

    private val completeAction = Action {
        KUtil.networkCellular = KUtil.getNetwork(this)
        KUtil.locationData = KUtil.getLocationData(this)

        val intent = Intent(this@RegisterActivity, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    private val errorAction = Consumer<Throwable> { error ->
        Log.e("SERVICE" , "error : =>> ${error.message}")
        if (error is HttpException) {
            // We had non-2XX http error
            if (error.response().errorBody() != null) {
                var message = ""
                try {
                    val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(),ErrorResponse::class.java)
                    message = erResponse.descriptions.toString()

                } catch (e: Exception) {
                    printlog.e("Parse json error from server.")
                    message = error.message()
                } finally {
                    KAlert.show(this,"Error!", message, {})
                }
            } else {
                ///
            }
        } else {
            ///
            KAlert.show(this,"Error!", "${error.message}", {})
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.pop_out,R.anim.pop_out_exit)
    }
}
