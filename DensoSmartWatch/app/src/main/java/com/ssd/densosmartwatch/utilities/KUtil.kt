package com.ssd.densosmartwatch.utilities

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.ssd.densosmartwatch.services.SensorRequest
import com.ssd.densosmartwatch.services.TokenResponse
import com.ssd.densosmartwatch.services.UserResponse
import kilmuz.kotlinlib.lib.KPreferences




object KUtil {

    var debug = true

    private val PREF_MACADDRESS = "mcaddress"
    private val PREF_TOKEN = "actoken"
    private val PREF_NETWORK = "network"
    private val PREF_LOCATION_DATA = "location_data"
    private val PREF_SENSOR_DATA = "sensor_u"

    val gson by lazy { GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create() }

    var userInfo:UserResponse? = null
    var acToken:TokenResponse? = null
    var mcAddress:String? = null
    var networkCellular:Boolean = true
    var locationData:Boolean = true
    var listsSensor:ArrayList<SensorRequest> = ArrayList()


    /// Sensor
    fun saveSensor(context: Context) {
        if (KUtil.userInfo?.UserId == null) return
        val json = gson.toJson(listsSensor)
        KPreferences.saveStrPref(context, "$PREF_SENSOR_DATA${KUtil.userInfo?.UserId}",json)
    }
    fun getSensor(context: Context) {
        if (KUtil.userInfo?.UserId == null) return
        KPreferences.getStrPref(context, "$PREF_SENSOR_DATA${KUtil.userInfo?.UserId}")?.let {
            try {
                KUtil.listsSensor =  gson.fromJson(it, object : TypeToken<ArrayList<SensorRequest>>() {}.type)
            } catch (e: Exception) {
                KUtil.listsSensor = ArrayList()
            }
        }
    }

    /// Network
    fun setNetwork(context: Context, isWiFi:Boolean) { KPreferences.saveBoolPref(context, PREF_NETWORK, isWiFi) }
    fun getNetwork(context: Context) : Boolean = context.getSharedPreferences(KPreferences.PREF_NAME, Context.MODE_PRIVATE).getBoolean(PREF_NETWORK, true)

    /// Location Data
    fun setLocationData(context: Context, isOn:Boolean) { KPreferences.saveBoolPref(context, PREF_LOCATION_DATA, isOn) }
    fun getLocationData(context: Context) : Boolean = context.getSharedPreferences(KPreferences.PREF_NAME, Context.MODE_PRIVATE).getBoolean(PREF_LOCATION_DATA, true)

    /// Token
    fun setToken(context:Context, token:TokenResponse) {
        val json = gson.toJson(token)
        KPreferences.saveStrPref(context, PREF_TOKEN,json)
    }
    fun getToken(context: Context) : TokenResponse? =
        KPreferences.getStrPref(context, PREF_TOKEN)?.let {
            try {
                gson.fromJson(it, TokenResponse::class.java)
            } catch (e: Exception) {
                null
            }
        } ?: run {
            null
        }

    fun deleteToken(context: Context) {
        KPreferences.deletePref(context, PREF_TOKEN)
    }


    /// Mac address ...
    fun setMacAddress(context:Context, mcAddress:String) {
        KPreferences.saveStrPref(context, PREF_MACADDRESS,mcAddress)
        this.mcAddress = mcAddress
    }
    fun getMacAddress(context: Context) : String? {
        mcAddress = KPreferences.getStrPref(context, PREF_MACADDRESS); return mcAddress
    }


    fun is3G(context: Context): Boolean {
        val manager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
            manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting
        else {
            val network =  manager.activeNetworkInfo
            if(network != null)
                (network.type == ConnectivityManager.TYPE_MOBILE)
            else
                false
        }
    }

    fun isWiFi(context: Context): Boolean {
        val manager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
            manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting
        else {
            val network =  manager.activeNetworkInfo
            if(network != null)
                (network.type == ConnectivityManager.TYPE_WIFI)
            else
                false
        }
    }


    fun printJSONFrom(any: Any) {
        val gs = GsonBuilder().setPrettyPrinting().create()
        Log.d("!-LOG-!",gs.toJson(any))
    }


}
