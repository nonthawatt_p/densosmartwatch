package com.ssd.densosmartwatch.adapter

import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.ssd.densosmartwatch.R
import com.ssd.densosmartwatch.WearableFragment
import java.util.*
import java.util.concurrent.TimeUnit


class ScanResultAdapter(private val mContext: Context, private val mInflater: LayoutInflater) : BaseAdapter() {
    private val mArrayList: ArrayList<ScanResult> = ArrayList()

    override fun getCount(): Int {
        return mArrayList.size
    }

    override fun getItem(position: Int): BluetoothDevice {
        return mArrayList[position].device
    }

    override fun getItemId(position: Int): Long {
        return mArrayList[position].device.address.hashCode().toLong()
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        var view = view
        // Reuse an old view if we can, otherwise create a new one.
        if (view == null) {
            view = mInflater.inflate(R.layout.adapter_list_wearable, null)
        }

        val tvName = view!!.findViewById<TextView>(R.id.device_name)
        val tvAddress = view!!.findViewById<TextView>(R.id.device_address)
//        val tvPacket = view!!.findViewById<TextView>(R.id.device_packet)
//        val tvLastSeen = view!!.findViewById<TextView>(R.id.device_seen)

        val scanResult = mArrayList[position]

        var name: String? = scanResult.device.name

        if (name == null) {
            name = "(no name)"
        }

        tvName.text = name
        tvAddress.text = scanResult.device.address
        val packet = (if (scanResult.scanRecord!!.getServiceData(WearableFragment.SERVICE_UUID) == null)
            scanResult.scanRecord!!.serviceData
        else
            scanResult.scanRecord!!.getServiceData(WearableFragment.SERVICE_UUID)).toString()
//        tvPacket.text = packet
//        tvLastSeen.text = getTimeSinceString(mContext, scanResult.timestampNanos)

        return view
    }

    /**
     * Search the adapter for an existing device address and return it, otherwise return -1.
     */
    private fun getPosition(address: String): Int {
        var position = -1
        for (i in mArrayList.indices) {
            if (mArrayList[i].device.address == address) {
                position = i

                break
            }
        }
        return position
    }

    /**
     * Add a ScanResult item to the adapter if a result from that device isn't already present.
     * Otherwise updates the existing position with the new ScanResult.
     */
    fun add(scanResult: ScanResult) {
        val existingPosition = getPosition(scanResult.device.address)

        if (existingPosition >= 0) {
            // Device is already in list, update its record.
            mArrayList[existingPosition] = scanResult
        } else {
            // Add new Device's ScanResult to list.
            mArrayList.add(scanResult)
        }
    }

    /**
     * Clear out the adapter.
     */
    fun clear() {
        mArrayList.clear()
    }

    companion object {

        /**
         * Takes in a number of nanoseconds and returns a human-readable string giving a vague
         * description of how long ago that was.
         */
        fun getTimeSinceString(context: Context, timeNanoseconds: Long): String {
            var result = "Last Seen: "

            val times = SystemClock.elapsedRealtimeNanos() - timeNanoseconds
            val seconds = TimeUnit.SECONDS.convert(times, TimeUnit.NANOSECONDS)

            if (seconds < 5) {
                result += "just now"
            } else if (seconds < 60) {
                result += seconds.toString() + " seconds ago"
            } else {
                val minutes = TimeUnit.MINUTES.convert(seconds, TimeUnit.SECONDS)

                if (minutes < 60) {
                    if (minutes == 1L) {
                        result += minutes.toString() + " minute ago"
                    } else {
                        result += minutes.toString() + " minutes ago"
                    }
                } else {
                    val hours = TimeUnit.HOURS.convert(minutes, TimeUnit.MINUTES)

                    if (hours == 1L) {
                        result += hours.toString() + " hour ago"
                    } else {
                        result += hours.toString() + " hours ago"
                    }
                }
            }

            return result
        }
    }
}