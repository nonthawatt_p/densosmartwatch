package com.ssd.densosmartwatch.services

import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import java.util.*


data class LoginRequest (
        var Email: String,
        var Password: String
)

data class RegisterRequest (
        @NonNull var Email: String,
        @NonNull var Password: String,
        @NonNull var ConfirmPassword: String
)

data class ResetRequest (
        @NonNull var Email:String
)


data class LoginResponse (
        @SerializedName("Login")
        var status : String? = null,
        var User: UserResponse? = null
)

data class SensorResponse (
        var data: SensorData? = null
)

data class SensorData (
        var UserId: Int? ,
        var PulseRate: Double? ,
        var Systolic: Int? ,
        var Diastolic: Int? ,
        var Abnomality: Boolean? ,
        var Reliability: Boolean? ,
        var CreateDate: Date? ,
        var Time: Long?
)


data class UserResponse (
//        var Name:String?,
//        var CompanyName:String?,
//        var CompanyAddress:String?,
//        var UserId:String?,
//        var PhoneNumber:String?
    var UserId: Int? = null,
    var Email: String? = null,
    var Gender: Boolean,
    var Age: Int? = null,
    var Height: Int? = null,
    var Weight: Int? = null,
    var Systolic: Int? = null,
    var Diastolic: Int? = null,
    var Password: String? = null

)

data class TokenResponse (
        var access_token: String?,
        var token_type: String?

//        @SerializedName("expires_in")
//        var ExpiredIn: Int?

//        @SerializedName("username")
//        var Username: String?
) {
        fun getAuth(): String? {
                if(token_type!=null && access_token!=null)
                        return token_type + " " + access_token
                else
                        return null
        }
}

data class ErrorResponse (
        @SerializedName("error")
        var title: String?,
        @SerializedName("error_description")
        var descriptions: String?,
        @SerializedName("Message")
        var message: String?
)

/*
 {
       "Message": "error message"
 }
*/

data class SensorRequest (
        var UserId: Int? ,
        var Pulse1: Short? ,
        var Pulse2: Short? ,
        var Pulse3: Short? ,
        var Pulse4: Short? ,
        var Pulse5: Short? ,
        var X: Int? ,
        var Y: Int? ,
        var Z: Int? ,
        var Latitude: Double? ,
        var Longitude: Double? ,
        var Time: Long?
)

data class BloodPressureData (var UserId: Int?)