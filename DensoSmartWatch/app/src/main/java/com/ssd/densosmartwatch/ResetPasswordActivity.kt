package com.ssd.densosmartwatch

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import com.ssd.densosmartwatch.services.DService
import com.ssd.densosmartwatch.services.ErrorResponse
import com.ssd.densosmartwatch.services.ResetRequest
import com.ssd.densosmartwatch.utilities.KUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kilmuz.kotlinlib.lib.KAlert
import kilmuz.kotlinlib.lib.hideKeyboard
import kotlinx.android.synthetic.main.activity_reset_password.*
import retrofit2.HttpException

class ResetPasswordActivity : KActivity() {

    var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        setToolbar(getString(R.string.reset_password),true)



        btn_reset_pass_submit.setOnClickListener {
            val email = edt_reset_pass_email.text.toString()
            if(email.isEmpty()) return@setOnClickListener

            progressDialog = ProgressDialog.show(this,null,getString(R.string.loading), true)
            hideKeyboard()
            DService.call.reset(ResetRequest(email))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnTerminate { progressDialog?.dismiss() }
                    .doOnComplete({
                        KAlert.show(this,R.string.success,R.string.alert_reset_password_complete,R.string.ok,{onBackPressed()})
                    })
                    .subscribe (Consumer {},errorAction)
        }


    }

    private val errorAction = Consumer<Throwable> { error ->
        Log.e("SERVICE" , "error : =>> ${error.message}")
        if (error is HttpException) {
            // We had non-2XX http error
            if (error.response().errorBody() != null) {
                val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(), ErrorResponse::class.java)
                KAlert.show(this,"Error!","${erResponse.message}", {})
            } else {
                ///
            }
        } else {
            ///
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.pop_out,R.anim.pop_out_exit)
    }
}
