package com.ssd.densosmartwatch

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Fragment
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.annotation.IdRes
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.LocationServices
import com.ssd.densosmartwatch.WearableFragment.Companion.SERVICE_UUID
import com.ssd.densosmartwatch.services.DService
import com.ssd.densosmartwatch.services.ErrorResponse
import com.ssd.densosmartwatch.services.SensorRequest
import com.ssd.densosmartwatch.utilities.KUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kilmuz.kotlinlib.lib.ObservingService
import kilmuz.kotlinlib.lib.checkCanSendData
import kilmuz.kotlinlib.lib.checkInternet
import kilmuz.kotlinlib.lib.printlog
import retrofit2.HttpException


class HomeActivity : KActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        com.google.android.gms.location.LocationListener {

    private var kBluetoothManager: BluetoothManager? = null
    private var kBluetoothAdapter: BluetoothAdapter? = null
    private var kBluetoothLeScanner: BluetoothLeScanner? = null
    private var kCallback: ScanCallback? = null
    private var fragmentLayout: Fragment? = null

    lateinit var tabLayout: TabLayout
    lateinit var navigationView: NavigationView
    lateinit var drawer: DrawerLayout

    private var kLocation: Location? = null
    private var isLandscape = -1
    private var isSendingData = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        printlog.d("oncreated home ac.... $isLandscape")
        setContentView(R.layout.activity_home)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        tabLayout = findViewById<TabLayout>(R.id.graph_tablayout)
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE)


        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        navigationView.setCheckedItem(R.id.nav_sensor)

        attachFragment(R.id.nav_sensor)

        Thread({
            kBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            kBluetoothAdapter = kBluetoothManager?.adapter
            kBluetoothLeScanner = kBluetoothAdapter?.bluetoothLeScanner
        }).start()

    }

    private fun attachFragment(@IdRes id:Int) {
        when (id) {
            R.id.nav_sensor -> { fragmentLayout = SensorFragment.newInstance() }
            R.id.nav_graph -> { fragmentLayout = GraphFragment.newInstance() }
            R.id.nav_profile -> { fragmentLayout = ProfileFragment.newInstance() }
            R.id.nav_settings -> { fragmentLayout = SettingsFragment.newInstance() }
            else -> {}
        }

        if(fragmentLayout != null) {
            if(id == R.id.nav_graph) {
                tabLayout.visibility = View.VISIBLE
            } else {
                tabLayout.visibility = View.GONE
            }
            val transaction = fragmentManager.beginTransaction()
            transaction.replace(R.id.frame_fragment, fragmentLayout)
            transaction.commit()
        }
        System.gc()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        isLandscape = newConfig?.orientation ?: -1
        printlog.d("on config home ac.... $isLandscape")

        if(newConfig?.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            /** If change device orientation to landscape. */
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            if(fragmentLayout is GraphFragment) {

                tabLayout.visibility = View.GONE
                supportActionBar?.hide()

                window.decorView.systemUiVisibility = (
//                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
            }
        } else {
            /** If change device orientation to portrait. */
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            window.decorView.systemUiVisibility = 0
            supportActionBar?.show()
            if(fragmentLayout is GraphFragment) {
                tabLayout.visibility = View.VISIBLE
            }
        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (fragmentLayout is SensorFragment) {
                super.onBackPressed()
                overridePendingTransition(R.anim.pop_out,R.anim.pop_out_exit)
            } else {
                attachFragment(R.id.nav_sensor)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        when (id) {
            R.id.nav_sensor -> // Handle the camera action
                attachFragment(id)
            R.id.nav_graph -> attachFragment(id)
            R.id.nav_profile -> attachFragment(id)
            R.id.nav_settings -> attachFragment(id)
            R.id.nav_logout -> logout()
            else -> {
                // skip
            }
        }
        if(checkCanSendData()) {
            queueSendData()
        }

        /** fixed lagging when closing drawer use delay. */
        Handler().postDelayed({
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            drawer.closeDrawer(GravityCompat.START)
        },50)

        return true
    }

    /**
     * Logout function. Use this when need to logout from app.
     * Declare from HomeActivity.
     */
    fun logout() {
        KUtil.userInfo = null
        KUtil.acToken = null
        KUtil.deleteToken(this)

        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    /**
     * Return a List of [ScanFilter] objects to filter by Service UUID.
     */
    private fun buildScanFilters(): List<ScanFilter> {
        val scanFilters = ArrayList<ScanFilter>()

        val builder = ScanFilter.Builder()
        // Comment out the below line to see all BLE devices around you
        //builder.setServiceUuid(SERVICE_UUID);
        scanFilters.add(builder.build())

        return scanFilters
    }

    /**
     * Return a [ScanSettings] object set to use low power (to preserve battery life).
     */
    private fun buildScanSettings(): ScanSettings {
        val builder = ScanSettings.Builder()
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)

        return builder.build()
    }

    /**
     * Custom ScanCallback object - adds to adapter on success, displays error on failure.
     */
    private inner class CustomScanCallback : ScanCallback() {
        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
            printlog.d("scan resultssss => $results")
        }

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            if (result.device.type == BluetoothDevice.DEVICE_TYPE_LE &&
//                    result.scanRecord?.deviceName == "NEXUS 6P"
                    result.device.address == KUtil.getMacAddress(this@HomeActivity)
                    ) {

//                printlog.d("scan result => $result")
//                val packet =result.scanRecord?.serviceUuids

                result.scanRecord?.serviceData?.let {
//                    Log.d("!-LOG-!","Bluetooth data2 => $it")
                    it[SERVICE_UUID]?.let {
                        parseData(it)
                    }
                }
            }
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Toast.makeText(this@HomeActivity, "Get data failed with error: " + errorCode, Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()

        KUtil.getSensor(this)

        /**
         * Check internet connection and call service to get UserInfo if it null in device.
         */
        if (KUtil.userInfo == null) {
            if(checkInternet()) {
                DService.call.userInfo(KUtil.acToken!!.getAuth()!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ t ->
                            KUtil.userInfo = t
                        }, {
                            KUtil.userInfo = null
                            KUtil.acToken = null

                            val intent = Intent(this, LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                            finish()
                        })
            } else {
                logout()
            }
        }

        // Declare callback of Scan Data from bluetooth.
        kCallback = CustomScanCallback()

        /**
         * Open new thread to call BLE Scanner. Protected lagging when open application.
         */
        Thread({
            kBluetoothLeScanner?.startScan(buildScanFilters(), buildScanSettings(), kCallback)
            if (kBluetoothAdapter != null) {
                // Is Bluetooth turned on?
                if (kBluetoothAdapter!!.isEnabled) {
                    checkLocation({
                        startLocationUpdates()
                    },null)
                } else {
                    val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH)
                }
            }
        }).start()
    }

    override fun onStop() {
        super.onStop()

        if(KUtil.listsSensor.size > 0) {
            KUtil.saveSensor(this)
        }

        /**
         * Clear BLE Scanner when pause or destroy application.
         * Turn off location update service.
         */
        kBluetoothLeScanner?.stopScan(kCallback)
        kCallback = null
        stopLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    fun parseData(bytes: ByteArray) {
        printlog.d("parseData = $bytes")
        val sb = StringBuilder()
        bytes.map { //            sb.append(String.format("%02X", b));
            it.toInt() and 255
        }.forEach {
            sb.append(String.format("%02X", it))
        }

        val wave1 = Integer.parseInt( sb.substring(2,6) , 16).toShort()
        val wave2 = Integer.parseInt( sb.substring(6,10)  , 16).toShort()
        val wave3 = Integer.parseInt( sb.substring(10,14) , 16).toShort()
        val wave4 = Integer.parseInt( sb.substring(14,18) , 16).toShort()
        val wave5 = Integer.parseInt( sb.substring(18,22) , 16).toShort()

        val accelX = Integer.parseInt( sb.substring(22,24) , 16)
        val accelY = Integer.parseInt( sb.substring(24,26) , 16)
        val accelZ = Integer.parseInt( sb.substring(26,28) , 16)

        printlog.d("1:$wave1 , 2:$wave2 , 3:$wave3 , 4:$wave4 , 5:$wave5 , acX:$accelX , acY:$accelY , acZ:$accelZ")
        printlog.d(sb.toString())


        kLocation?.let {
            LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        }

        try {
            val systemTime = System.currentTimeMillis()
            printlog.i("System Time =>> $systemTime")

            val lati = if(KUtil.locationData) kLocation?.latitude else null
            val longi = if(KUtil.locationData) kLocation?.longitude else null

            val waveData = SensorRequest( KUtil.userInfo?.UserId, wave1,wave2,wave3,wave4,wave5,
                    accelX,accelY,accelZ,lati, longi, systemTime )

            KUtil.listsSensor.add(waveData)

            ObservingService.postNotification("BLE_DATA",waveData)

            /**
             * Check if data is WiFi or Cellular.
             * This not need to show dialog if can't connection internet.
             */
            if(checkCanSendData()) {
                // todo send data to server...
//                queueSendData()
                printlog.d("Can send data...")
            } else {
                // Not connect to internet
                printlog.d("Cannot send data...")

            }

        } catch (e: Exception) {
            printlog.e(" ${e.localizedMessage}")
        }
    }

    private fun queueSendData() {
        if(isSendingData)return
        if(KUtil.listsSensor.size <= 0) return
        isSendingData = true

        val sendingList = KUtil.listsSensor.clone() as ArrayList<SensorRequest>
        /// call service....
        DService.call.sendSensorData(KUtil.acToken!!.getAuth()!!, sendingList)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnTerminate { isSendingData = false }
                .doOnComplete {
                    printlog.i("Send data complete!")
                    KUtil.listsSensor.removeAll(sendingList)
                }
                .subscribe(Consumer{ t ->  },errorAction)
    }

    /**
     * Handle error action when call service API response is error.
     */
    private val errorAction = Consumer<Throwable> { error ->
        Log.e("SERVICE" , "error : =>> ${error.message}")
        if (error is HttpException) {
            // We had non-2XX http error
            if (error.response().errorBody() != null) {
                var message = ""
                try {
                    val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(), ErrorResponse::class.java)
                    message = erResponse.descriptions.toString()

                } catch (e: Exception) {
                    printlog.e("Parse json error from server.")
                    message = error.message()
                } finally {
                    printlog.e(message)
                }
            } else {
                /// TODO If error body is null
            }
        } else {
            /// TODO If error isn't HttpException
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        initLocationRequest()

        try {
            if(mGoogleApiClient != null && mLocationRequest != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            }
        } catch (e: Exception) {
            printlog.e(e.localizedMessage)
        }
    }

    private fun stopLocationUpdates() {
        try {
            if (mGoogleApiClient != null && mGoogleApiClient?.isConnected!!) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            }
        } catch (e: Exception) {
            printlog.e(e.localizedMessage)
        }
    }

    /**
     * Implement onLocationChanged from Google Location Listener.
     */
    override fun onLocationChanged(location: Location?) {
        kLocation = location
        printlog.i("location : ${location?.latitude} , ${location?.longitude}")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        when (requestCode) {
            REQUEST_ENABLE_BLUETOOTH -> when (resultCode) {
                Activity.RESULT_OK -> {
                    checkLocation({
                        startLocationUpdates()
                    })
                }
                Activity.RESULT_CANCELED -> {

                    //TODO Bluetooth canceled

                    return
                }
                else -> {
                }
            }
            REQUEST_ENABLE_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                    // All required changes were successfully made
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> {
                    // The user was asked to change settings, but chose not to
                    //TODO Location canceled

                }
                else -> {
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_COARSE_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    println("Coarse Location Permission granted")
                    startLocationUpdates()
                } else {

                    //TODO Location Permission Canceled

                    return
                }
                return
            }
        }
    }

}
