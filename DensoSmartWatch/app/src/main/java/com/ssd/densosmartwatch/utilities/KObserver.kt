package kilmuz.kotlinlib.lib

import java.util.*

/**
 * Created by KiLMuZ on 9/28/2017 AD.
 */
object ObservingService {
//    internal var observables: HashMap<String, CustomObservable> = HashMap()
//    companion object {
        var observables: HashMap<String, CustomObservable> = HashMap()
//    }

    fun addObserver(notification: String, observer: Observer) {
        var observable = observables[notification]
        if (observable == null) {
            observable = CustomObservable()
            observables.put(notification, observable)
        }
        observable.addObserver(observer)
    }

    fun removeObserver(notification: String, observer: Observer) {
        val observable = observables[notification]
        if (observable != null) {
            observable.deleteObserver(observer)
        }
    }

    fun postNotification(notification: String, obj: Any) {
        val observable = observables[notification]
        if (observable != null) {
            observable.notifyObservers(obj)
        }
    }
}

class CustomObservable : Observable() {
    // To force notifications to be sent
    override fun notifyObservers(data: Any) {
        setChanged()
        super.notifyObservers(data)
    }
}