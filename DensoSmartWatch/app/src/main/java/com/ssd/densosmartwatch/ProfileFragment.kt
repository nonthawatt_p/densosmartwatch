package com.ssd.densosmartwatch

import android.app.Fragment
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.ssd.densosmartwatch.services.DService
import com.ssd.densosmartwatch.services.ErrorResponse
import com.ssd.densosmartwatch.services.ResetRequest
import com.ssd.densosmartwatch.utilities.KUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kilmuz.kotlinlib.lib.KAlert
import kilmuz.kotlinlib.lib.checkInternet
import kilmuz.kotlinlib.lib.hideKeyboard
import kilmuz.kotlinlib.lib.printlog
import retrofit2.HttpException


class ProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    var progressDialog : ProgressDialog? = null
    var strMale = ""
    var strFemale = ""

    private lateinit var edtEmail:EditText
    private lateinit var edtWearable:EditText
    private lateinit var edtGender:EditText
    private lateinit var edtAge:EditText
    private lateinit var edtWeight:EditText
    private lateinit var edtHeight:EditText
    private lateinit var edtDiastolic:EditText
    private lateinit var edtSystolic:EditText

    private lateinit var btnReset:Button
    private lateinit var btnSubmit:Button

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater!!.inflate(R.layout.fragment_profile, container, false)
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity.title = getString(R.string.menu_profile)

        strMale = getString(R.string.male)
        strFemale = getString(R.string.female)

        edtEmail = view.findViewById(R.id.edtpro_email)
        edtWearable = view.findViewById(R.id.edtpro_wearable)
        edtGender = view.findViewById(R.id.edtpro_gender)
        edtAge = view.findViewById(R.id.edtpro_age)
        edtWeight = view.findViewById(R.id.edtpro_weight)
        edtHeight = view.findViewById(R.id.edtpro_height)
        edtDiastolic = view.findViewById(R.id.edtpro_diastolic)
        edtSystolic = view.findViewById(R.id.edtpro_systolic)

        btnReset = view.findViewById<Button>(R.id.btnpro_reset)
        btnSubmit = view.findViewById<Button>(R.id.btnpro_submit)

        if(activity.checkInternet()) {
            progressDialog = ProgressDialog.show(activity,null,getString(R.string.loading), true)
            if (KUtil.acToken != null) {
                DService.call.userInfo(KUtil.acToken!!.getAuth()!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnTerminate { progressDialog?.dismiss();setEdt() }
                        .subscribe({ user ->
                            KUtil.printJSONFrom(user)
                            KUtil.userInfo = user
                        }, {})
            } else {
                printlog.e("Don't have access token... ${KUtil.acToken}")
                progressDialog?.dismiss()
            }
        } else {
            setEdt()
        }

        edtGender.setOnClickListener({
            val choice = arrayOf<CharSequence>(strMale, strFemale)
            AlertDialog.Builder(activity)
                    .setTitle(getString(R.string.choose_your_gender))
                    .setSingleChoiceItems(choice, 0, null)
                    .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, whichButton ->
                        dialog.dismiss()
                        val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                        when (selectedPosition) {
                            0 -> {edtGender.setText(choice[0])}
                            1 -> {edtGender.setText(choice[1])}
                            else -> {}
                        }
                        // Do something useful withe the position of the selected radio button
                    })
                    .setNegativeButton(R.string.cancel, { dialog, whichButton ->
                        dialog.dismiss()
                    })
                    .setCancelable(false)
                    .show()
        })

        btnReset.setOnClickListener {
            if(!activity.checkInternet()) { return@setOnClickListener }
            if (edtEmail.text.toString() != KUtil.userInfo?.Email) {
                KAlert.showWithOK(activity,R.string.warning,"Email is not match", {})
                return@setOnClickListener
            } else {
                confirmResetPassword()
            }
        }

        btnSubmit.setOnClickListener {
            activity.hideKeyboard()
            if(!activity.checkInternet()) { return@setOnClickListener }
            KAlert.showCustomEdt(activity,{ edt, dialog ->
                activity.hideKeyboard()
                if(!edt.text.isEmpty()) {
                    val passw = edt.text.toString()
                    dialog.dismiss()
                    progressDialog = ProgressDialog.show(activity,null,getString(R.string.loading), true)

                    KUtil.userInfo?.Email = edtEmail.text.toString()
                    KUtil.userInfo?.Gender = edtGender.text.toString()==strMale
                    KUtil.userInfo?.Age = edtAge.text.toString().toInt()
                    KUtil.userInfo?.Height = edtHeight.text.toString().toInt()
                    KUtil.userInfo?.Weight = edtWeight.text.toString().toInt()
                    KUtil.userInfo?.Diastolic = edtDiastolic.text.toString().toInt()
                    KUtil.userInfo?.Systolic = edtSystolic.text.toString().toInt()
                    KUtil.userInfo?.Password = passw

                    val token = KUtil.acToken ?: return@showCustomEdt
                    val userInfo = KUtil.userInfo ?: return@showCustomEdt
                    DService.call.userInfoChange(token.getAuth()!!, userInfo)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnTerminate { progressDialog?.dismiss() }
                            .doOnComplete({
                                /// update success
                                KAlert.show(activity,R.string.success,R.string.alert_update_profile_complete,R.string.ok,null)
                            })
                            .subscribe( Consumer{ response -> printlog.d(response?.string()) }, errorAction)
                } else {
                    edt.requestFocus()
                }
            })
        }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

    }

    private fun setEdt() {
        edtEmail.setText(KUtil.userInfo?.Email)
        edtWearable.setText(KUtil.mcAddress)

        edtGender.setText( if(KUtil.userInfo?.Gender != null) if(KUtil.userInfo?.Gender!!) strMale else strFemale else "")
        edtAge.setText(KUtil.userInfo?.Age?.toString())
        edtWeight.setText(KUtil.userInfo?.Height?.toString())
        edtHeight.setText(KUtil.userInfo?.Weight?.toString())
        edtDiastolic.setText(KUtil.userInfo?.Diastolic?.toString())
        edtSystolic.setText(KUtil.userInfo?.Systolic?.toString())
    }

    private fun confirmResetPassword() {
        KAlert.showWithOKAndCancelButton(activity,R.string.confirm,R.string.alert_reset_password_comfirm, {
            onOK {
                progressDialog = ProgressDialog.show(activity,null,getString(R.string.loading), true)
                // Call service reset password.
                DService.call.reset(ResetRequest(edtEmail.text.toString()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnTerminate { progressDialog?.dismiss() }
                        .doOnComplete({
                            /// Reset success
                            KAlert.showWithOK(activity,R.string.success,R.string.alert_reset_password_complete, { dialogInterface ->
                                (activity as HomeActivity).logout()
                            })
                        })
                        .subscribe({ response -> printlog.d(response?.string()) }, {  })
            }

            onCancel { dialogInterface -> dialogInterface.dismiss() }
        })
    }

    // Handle error from service
    private val errorAction = Consumer<Throwable> { error ->
        Log.e("SERVICE" , "error : =>> ${error.message}")
        if (error is HttpException) {
            // We had non-2XX http error
            if (error.response().errorBody() != null) {
                var message = ""
                try {
                    val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(), ErrorResponse::class.java)
                    message = erResponse.descriptions.toString()

                } catch (e: Exception) {
                    printlog.e("Parse json error from server.")
                    message = error.message()
                } finally {
                    KAlert.show(activity,"Error!", message, {})
                }
            } else {
                ///
            }
        } else {
            ///
            KAlert.show(activity,"Error!", "${error.message}", {})
        }
    }
}