package com.ssd.densosmartwatch

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.ssd.densosmartwatch.services.DService
import com.ssd.densosmartwatch.services.ErrorResponse
import com.ssd.densosmartwatch.services.LoginRequest
import com.ssd.densosmartwatch.services.TokenResponse
import com.ssd.densosmartwatch.utilities.KUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kilmuz.kotlinlib.lib.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.HttpException

class LoginActivity : AppCompatActivity() {

    var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        checkInternet()
//        edtlogin_username.setText("kilmus@live.com")
//        edtlogin_passw.setText("Welcome@1")

        tvlogin_resetpassw.setOnClickListener {
            goActivity(ResetPasswordActivity::class.java)
        }

        tvlogin_register.setOnClickListener {
            goActivity(RegisterActivity::class.java)
        }

        btn_login.setOnClickListener {
            hideKeyboard()


            /** Check field is empty. */
            if (edtlogin_username.text.isNullOrEmpty() && edtlogin_passw.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@LoginActivity, R.string.warning, R.string.alert_pls_input_email_passw, null)
                return@setOnClickListener
            }
            if (edtlogin_username.text.isNullOrEmpty() ) {
                KAlert.showWithOK(this@LoginActivity, R.string.warning, R.string.alert_pls_input_email, null)
                return@setOnClickListener
            }
            if (edtlogin_passw.text.isNullOrEmpty()) {
                KAlert.showWithOK(this@LoginActivity, R.string.warning, R.string.alert_pls_input_passw, null)
                return@setOnClickListener
            }

            /** Check email password format. */
            if (!edtlogin_username.isEmailValid()) {
                KAlert.showWithOK(this@LoginActivity, R.string.warning, R.string.alert_email_invalid, null)
                return@setOnClickListener
            }
            if(edtlogin_passw.text.length<8) {
                KAlert.show(this@LoginActivity,getString(R.string.warning),getString(R.string.alert_passw_should_min_8_digit), {})
                return@setOnClickListener
            }

            /** Check internet. */
            if(!checkInternet()) { return@setOnClickListener }

            /** If field is pass all condition. Show progress dialog and call service. */
            progressDialog = ProgressDialog.show(this,null,getString(R.string.loading), true)
            DService.call.token(edtlogin_username.text.toString(),edtlogin_passw.text.toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnNext { token ->
                        // Call service 'UserInfo' to get user data.
                        KUtil.printJSONFrom(token)
                        callUserInfo(token)
                    }
                    .doOnError {
                        progressDialog?.dismiss()
                    }
                    .subscribe (Consumer {},errorAction)
        }

        btn_login.setOnLongClickListener {

            /** If field is pass all condition. Show progress dialog and call service. */
            progressDialog = ProgressDialog.show(this,null,getString(R.string.loading), true)
            DService.call.login(LoginRequest("kilmus@live.com","Welcome@1"))//.token("kilmus@live.com","Welcome@1")

                    .flatMap { item ->
                        KUtil.acToken = TokenResponse("-pFqKJ5iUpeDH9OxfedrYLGm2rZ0GrfWpIthq5-_7r9Qwd_r62s_1oqHRaBUmyhVoX8T_bjV9RaAyVevhy42tTuw5yozJclu9-mOzgokoUCD2Q3mtG5meoaoCYpqHG7N9kVf35be6BiXFftxbH4WxSI2OGZKJvCtbRq5ytBrU9sHqDdHBh7Yp0SIChCVkAgtD-Z6wlCadG83RY3ttms4BPNWrnJBtixokV4uUySGPEFfj2k2wWPZmskhO1ABjSVW4hLXEaeGZps8Cz04w41g0ejST3AzMSxOnPj1Ud7P3B__J8Cfdc6m0pliCF3pMzUmyegLSFouuR6-PKNd9UMjCTg8BKYvtEpb-SYLhGiewqQ0TzezkmhN7Ttv2PGC_hYFMtIt38XoFlYUd1XGj1OuWl1NGgixonwQq3J__TsPRPmnmD7mGWIjPlago7lemc5cH_YG2qI7suumUQZF12gB8ffAxODiKc7O7d1Woor33r0DIWV1_F5EuyAaSZnVOCQ-UxfSeM4cWH65y4WMmplEt1tOXxe_spfpc8IPrqTEHCATEJPVo4Yn80mBzvAPHBJj8Aw7Rfgb7NYSlSsUOUzxjqZYhX5uu_uLiCLlU4We178ZSMlVvekPKtdS6QiajIFY0CTlVnwy-Gd9Nba0s7k8eg", "bearer")
                        KUtil.setToken(this, KUtil.acToken!!)
                        return@flatMap DService.call.userInfo(KUtil.acToken!!.getAuth()!!)
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnTerminate { progressDialog?.dismiss() }
                    .doOnNext { user -> /*KUtil.userInfo = user*/ }
                    .doOnComplete(completeAction)
                    .subscribe(Consumer { user -> KUtil.printJSONFrom(user) },errorAction)

            return@setOnLongClickListener true
        }
    }

    private fun callUserInfo(tokens: TokenResponse) {
        KUtil.acToken = tokens
        KUtil.setToken(this, tokens)
        DService.call.userInfo(tokens.getAuth()!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnTerminate { progressDialog?.dismiss() }
                .doOnNext { user -> KUtil.userInfo = user }
                .doOnComplete(completeAction)
                .subscribe(Consumer { user -> KUtil.printJSONFrom(user) },errorAction)
    }

    private val errorAction = Consumer<Throwable> { error ->
        Log.e("SERVICE" , "error : =>> ${error.message}")
        if (error is HttpException) {
            // We had non-2XX http error
            if (error.response().errorBody() != null) {
                var message = ""
                try {
                    val erResponse = KUtil.gson.fromJson(error.response().errorBody()!!.string(),ErrorResponse::class.java)
                    message = erResponse.descriptions.toString()

                } catch (e: Exception) {
                    printlog.e("Parse json error from server.")
                    message = error.message()
                } finally {
                    KAlert.show(this,"Error!", message, {})
                }
            } else {
                ///
            }
        } else {
            ///
            KAlert.show(this,"Error!", "${error.message}", {})
        }
    }

    private val completeAction = Action {
        KUtil.networkCellular = KUtil.getNetwork(this)
        KUtil.locationData = KUtil.getLocationData(this)

        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    override fun onStop() {
        super.onStop()
        DService.disposable?.dispose()
    }

}
