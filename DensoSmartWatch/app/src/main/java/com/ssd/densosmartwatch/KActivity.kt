package com.ssd.densosmartwatch

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.head_toolbar.*



open class KActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener  {

    val REQUEST_ENABLE_BLUETOOTH = 1
    val REQUEST_ENABLE_LOCATION = 2
    val REQUEST_COARSE_LOCATION = 1

    var mGoogleApiClient: GoogleApiClient? = null
    var mLocationRequest: LocationRequest? = null
    var mLocationResult: PendingResult<LocationSettingsResult>? = null

    var completionCallback :( () -> Unit )? = null
    var failureCallback :( () -> Unit )? = null

    protected fun setToolbar(title:String, isBack:Boolean) {
        kToolbar.title = title
        setSupportActionBar(kToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(isBack)
        kToolbar.setNavigationOnClickListener({ onBackPressed() })
    }

    protected fun setCustomToolbar(@IdRes resId:Int, title:String, isBack: Boolean) {
        val toolbar = findViewById<Toolbar>(resId)
        toolbar.title = title
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(isBack)
        toolbar.setNavigationOnClickListener({ onBackPressed() })
    }

    protected fun initGoogleAPIClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    protected fun checkLocation( completion:( () -> Unit )? , failure: ( () -> Unit )? = null) {
        completionCallback = completion
        failureCallback = failure
        initGoogleAPIClient()
        mGoogleApiClient!!.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        ///
//        checkLocationPermission()
        failureCallback?.invoke()
    }

    override fun onConnected(p0: Bundle?) {
        checkLocationPermission()
    }

    override fun onConnectionSuspended(p0: Int) {
        /// Suspend
    }

    protected fun checkLocationPermission() {
        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                val builder = AlertDialog.Builder(this)
//                builder.setTitle("Location Permission")
//                builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.")
//                builder.setPositiveButton(android.R.string.yes) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_COARSE_LOCATION)
                    } else {
                        ActivityCompat.requestPermissions(this@KActivity,arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_COARSE_LOCATION)
                    }
//                }
//                builder.setNegativeButton(android.R.string.no, null)
//
//                builder.show()
            } else {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val isGpsProviderEnabled: Boolean = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                val isNetworkProviderEnabled: Boolean = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)


                if (!isGpsProviderEnabled && !isNetworkProviderEnabled) {
                    turnOnLocation()
//                    val builder = AlertDialog.Builder(this)
//                    builder.setTitle("Location not enabled")
//                    builder.setMessage("The app needs location data. Please enable location to continue using the features of the app.")
//                    builder.setPositiveButton(android.R.string.yes) { dialogInterface, i ->
//                        // enable location
//                        turnOnLocation()
//                    }
//                    builder.setNegativeButton(android.R.string.no, null)
//
//                    builder.show()
                } else {
                    completionCallback?.invoke()
                }
            }
        }
    }

    protected fun initLocationRequest() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.interval = (30 * 1000).toLong()
        mLocationRequest!!.fastestInterval = (5 * 1000).toLong()
    }

    protected fun turnOnLocation() {
        initLocationRequest()

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)

        mLocationResult = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        mLocationResult!!.setResultCallback({ result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();

            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> { completionCallback?.invoke() }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult(). !!!!!
                        status.startResolutionForResult(this@KActivity, REQUEST_ENABLE_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> { failureCallback?.invoke() }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        })
    }



    /*override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.pop_out,R.anim.pop_out_exit)
    }*/
}