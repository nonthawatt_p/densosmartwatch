package kilmuz.kotlinlib.lib

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Created by KiLMuZ on 9/28/2017 AD.
 */

object KPreferences {

    val PREF_NAME = "denso.prefs"
    @JvmField var gson: Gson? = null

    /// Preference setting.
    @JvmStatic fun saveBoolPref(context:Context, key: String, value:Boolean) {
        val prefEditor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        prefEditor.putBoolean(key,value)
        prefEditor.apply()
    }
    @JvmStatic fun getBoolPref(context:Context, key: String) : Boolean = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getBoolean(key, false)

    @JvmStatic fun saveStrPref(context:Context, key:String, value: String) {
        val prefEditor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        prefEditor.putString(key,value)
        prefEditor.apply()
    }
    @JvmStatic fun getStrPref(context:Context, key: String) : String? = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(key, null)

    @JvmStatic fun deletePref(context: Context, key: String) {
        val prefEditor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        prefEditor.remove(key)
        prefEditor.apply()
    }

    /// GSON Util
    @JvmStatic fun gson(): Gson {
        if(KPreferences.gson == null)
            gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
        return KPreferences.gson!!
    }

}