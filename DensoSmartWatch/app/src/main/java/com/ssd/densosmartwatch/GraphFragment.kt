package com.ssd.densosmartwatch

import android.app.Fragment
import android.app.FragmentManager
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v13.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ssd.densosmartwatch.utilities.LockableViewPager

class GraphFragment : Fragment() {

    companion object {
        fun newInstance() = GraphFragment()
    }
//    private var mListener: GraphFragment.OnFragmentInteractionListener? = null
    lateinit var pager: LockableViewPager

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater!!.inflate(R.layout.fragment_graph, container, false)
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        activity.title = getString(R.string.menu_graph)

        return view
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        pager.swipeLocked = newConfig?.orientation == Configuration.ORIENTATION_LANDSCAPE
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val pageAdapter = PageAdapter(activity, childFragmentManager)
        pager = view!!.findViewById<LockableViewPager>(R.id.graph_pager)
//        pager.swipeLocked = true
        pager.adapter = pageAdapter
        (activity as HomeActivity).tabLayout.setupWithViewPager(pager)
        (activity as HomeActivity).tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                tab?.let { // EzIce said ... Press button to load URL.
                    when (it.position) {
                        0 -> {
                            (pageAdapter.getItem(it.position) as GraphPulseFragment).let {
                                it.loadURL()
                            }
                        }
                        1 -> {
                            (pageAdapter.getItem(it.position) as GraphPressureFragment).let {
                                it.loadURL()
                            }
                        }
                        else -> {}
                    }

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                // called from tab select or scroll to
            }
        })
    }

    class PageAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var tabs = arrayOf(
                GraphPulseFragment.newInstance(),
                GraphPressureFragment.newInstance()
        )

        override fun getItem(position: Int): Fragment? {
            return tabs[position]
        }

        override fun getPageTitle(position: Int): CharSequence {
            // Generate title based on item position. This set on TabLayout.
            return when(position) {
                0 -> "Pulse Rate"
                1 -> "Blood Pressure"
                else -> ""
            }
        }

        override fun getCount(): Int {
            return tabs.size
        }
    }
}
