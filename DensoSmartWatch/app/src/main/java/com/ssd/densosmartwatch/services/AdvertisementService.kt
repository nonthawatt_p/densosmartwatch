package com.ssd.densosmartwatch.services

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.bluetooth.BluetoothManager
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.ParcelUuid
import android.widget.Toast
import com.ssd.densosmartwatch.LoginActivity
import com.ssd.densosmartwatch.R
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Nachanok on 9/13/2017.
 */

class AdvertisementService : Service() {

    val SERVICE_UUID = ParcelUuid.fromString("0000b81d-0000-1000-8000-00805f9b34fb")

    private var mBluetoothLeAdvertiser: BluetoothLeAdvertiser? = null

    private var mAdvertiseCallback: AdvertiseCallback? = null

    private var mHandler: Handler? = null
    private val mAdvertiseHandler: Handler? = null

    private var mRunnable: Runnable? = null
    private val mAdvertiseRunnable: Runnable? = null

    private var mRandom: Random? = null

    override fun onCreate() {
        mRunning = true

        initialize()

        start()

        //setTimeout();

        super.onCreate()
    }

    override fun onDestroy() {
        /**
         * Note that onDestroy is not guaranteed to be called quickly or at all. Services exist at
         * the whim of the system, and onDestroy can be delayed or skipped entirely if memory need
         * is critical.
         */
        mRunning = false

        stop()

        //mHandler.removeCallbacks(mRunnable);

        stopForeground(true)

        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    /**
     * Get references to system Bluetooth objects if we don't have them already.
     */
    private fun initialize() {
        if (mBluetoothLeAdvertiser == null) {
            val mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager

            val mBluetoothAdapter = mBluetoothManager.adapter

            if (mBluetoothAdapter != null) {
                mBluetoothLeAdvertiser = mBluetoothAdapter.bluetoothLeAdvertiser
                mRandom = Random()
            } else {
                Toast.makeText(this, "Error: Bluetooth object null", Toast.LENGTH_LONG).show()
            }

        }
    }

    /**
     * Starts a delayed Runnable that will cause the BLE Advertising to timeout and stop after a
     * set amount of time.
     */
    private fun setTimeout() {
        mHandler = Handler()

        mRunnable = Runnable {
            send(ADVERTISING_TIMED_OUT)

            stopSelf()
        }

        mHandler!!.postDelayed(mRunnable, TIME_OUT)
    }

    /**
     * Starts BLE Advertising.
     */
    private fun start() {
        foreground()

        if (mAdvertiseCallback == null) {
            mAdvertiseCallback = CustomAdvertiseCallback()
            //mAdvertiseHandler = new Handler();

            //mAdvertiseRunnable = new Runnable() {
            //    @Override
            //    public void run() {
            val settings = Settings()
            val data = Data()

            if (mBluetoothLeAdvertiser != null) {
                mBluetoothLeAdvertiser!!.startAdvertising(settings, data, mAdvertiseCallback)
                //            mAdvertiseHandler.postDelayed(mAdvertiseRunnable, 30000);
            }
            //    }
            //};

            //mAdvertiseHandler.post(mAdvertiseRunnable);
        }
    }

    /**
     * Move service to the foreground, to avoid execution limits on background processes.
     *
     *
     * Callers should call stopForeground(true) when background work is complete.
     */
    private fun foreground() {
        val intent = Intent(this, LoginActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val notification = Notification.Builder(this)
                .setContentTitle("Advertising device via Bluetooth")
                .setContentText("This device is discoverable to others nearby.")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build()

        startForeground(FOREGROUND_NOTIFICATION_ID, notification)
    }

    /**
     * Stops BLE Advertising.
     */
    private fun stop() {
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser!!.stopAdvertising(mAdvertiseCallback)
            //mAdvertiseHandler.removeCallbacks(mAdvertiseRunnable);

            mAdvertiseCallback = null
            //mAdvertiseHandler = null;
        }
    }

    /**
     * Returns an AdvertiseData object which includes the Service UUID and Device Name.
     */
    private fun Data(): AdvertiseData {
        /**
         * Note: There is a strict limit of 31 Bytes on packets sent over BLE Advertisements.
         * This includes everything put into AdvertiseData including UUIDs, device info, &
         * arbitrary service or manufacturer data.
         * Attempting to send packets over this limit will result in a failure with error code
         * AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE. Catch this error in the
         * onStartFailure() method of an AdvertiseCallback implementation.
         */

        val builder = AdvertiseData.Builder()
        builder.addServiceUuid(SERVICE_UUID)
        //builder.setIncludeDeviceName(true);

        /* For example - this will cause advertising to fail (exceeds size limit) */
        //String failureData = "asdghkajsghalkxcjhfa;sghtalksjcfhalskfjhasldkjfhdskf";
        //dataBuilder.addServiceData(SERVICE_UUID, failureData.getBytes());

        val bytes = ByteArray(14)
        mRandom!!.nextBytes(bytes)

        builder.addServiceData(SERVICE_UUID, "Data".toByteArray())

        return builder.build()
    }

    /**
     * Returns an AdvertiseSettings object set to use low power (to help preserve battery life)
     * and disable the built-in timeout since this code uses its own timeout runnable.
     */
    private fun Settings(): AdvertiseSettings {
        val builder = AdvertiseSettings.Builder()
        builder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
        builder.setTimeout(0)

        return builder.build()
    }

    /**
     * Custom callback after Advertising succeeds or fails to start. Broadcasts the error code
     * in an Intent to be picked up by AdvertiserFragment and stops this Service.
     */
    private inner class CustomAdvertiseCallback : AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)

            send(errorCode)

            stopSelf()
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            super.onStartSuccess(settingsInEffect)
        }
    }

    /**
     * Builds and sends a broadcast intent indicating Advertising has failed. Includes the error
     * code as an extra. This is intended to be picked up by the `AdvertiserFragment`.
     */
    private fun send(errorCode: Int) {
        val intent = Intent()
        intent.action = ADVERTISING_FAILED
        intent.putExtra(ADVERTISING_FAILED_EXTRA_CODE, errorCode)

        sendBroadcast(intent)
    }

    companion object {
        private val TAG = AdvertisementService::class.java.simpleName
        private val FOREGROUND_NOTIFICATION_ID = 1
        val ADVERTISING_FAILED = "com.ssd.bleadvertisementresearch.advertising_failed"
        val ADVERTISING_FAILED_EXTRA_CODE = "failureCode"
        val ADVERTISING_TIMED_OUT = 6
        /**
         * Length of time to allow advertising before automatically shutting off. (10 minutes)
         */
        private val TIME_OUT = TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES)

        var mRunning = false
    }
}

