package com.ssd.densosmartwatch.services

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


class DService {
    companion object {
        private val URLs = "http://192.168.1.85:8085/"
//        private val URLs = ""


        val call by lazy {
            DService.create()
        }
        var disposable: Disposable? = null

        private fun create(): DensoService {
            val client = OkHttpClient().newBuilder().build()
//                    .addInterceptor(HttpLoggingInterceptor().apply {
//                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
//                    })
//                    .build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
//                    .client(client)
                    .baseUrl(URLs)
                    .build()

            return retrofit.create(DensoService::class.java)
        }



    }
}

interface DensoService {

    @POST("api/Account/LoginByEmail")
    fun login(@Body request:LoginRequest) : Observable<LoginResponse>

    @FormUrlEncoded
    @POST("authenticate")
    fun token(@Field("username") username: String, @Field("password") password: String, @Field("grant_type") type: String = "password"): Observable<TokenResponse>

    @GET("api/Account/UserInfo")
    fun userInfo(@Header("Authorization") authorize: String): Observable<UserResponse>

    @POST("api/Account/UserInfo")
    fun userInfoChange(@Header("Authorization") authorize: String,@Body userResponse: UserResponse): Observable<ResponseBody?>

    @POST("api/Account/Register")
    fun register(@Body registerRequest: RegisterRequest): Observable<ResponseBody?>

    @POST("api/Account/ForgotPassword")
    fun reset(@Body req: ResetRequest): Observable<ResponseBody?>

    @POST("api/Log/ReceiveSensor")
    fun sendSensorData(@Header("Authorization") authorize: String,@Body sensorRequestLists: ArrayList<SensorRequest>): Observable<SensorResponse?>

}