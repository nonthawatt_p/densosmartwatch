package com.ssd.densosmartwatch

import android.app.Fragment
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.github.lzyzsd.circleprogress.ArcProgress
import com.google.android.gms.location.LocationRequest
import kilmuz.kotlinlib.lib.ObservingService
import kilmuz.kotlinlib.lib.isNetworkConnected
import java.util.*




class SensorFragment : Fragment() {


    private lateinit var pbHeartRate: ArcProgress
    private lateinit var pbDiastolic: ProgressBar
    private lateinit var pbSystolic: ProgressBar
    private lateinit var tvHR: TextView
    private lateinit var tvDias: TextView
    private lateinit var tvSys: TextView

    companion object {
        fun newInstance() : SensorFragment = SensorFragment()
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater!!.inflate(R.layout.fragment_sensor, container, false)
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity.title = getString(R.string.menu_sensor)

        pbHeartRate = view!!.findViewById(R.id.progress_heartrate)
        pbDiastolic = view!!.findViewById(R.id.progress_diastolic)
        pbSystolic = view!!.findViewById(R.id.progress_systolic)

        tvHR = view!!.findViewById(R.id.tv_sensor_hr)
        tvDias = view!!.findViewById(R.id.tv_sensor_dias)
        tvSys = view!!.findViewById(R.id.tv_sensor_sys)

//
//        pbHeartRate.progress = 50
//        val rotateDrawable = pbHeartRate.indeterminateDrawable as RotateDrawable
//        rotateDrawable.toDegrees = 277f
//
//
//        pbHeartRate.clearAnimation()

        return view
    }

    private val runTimeRandom = Runnable {
        val randd = Random()
        val doubl = randd.nextInt(40) + 50
        pbDiastolic.progress = doubl
        tvDias.text = "$doubl"

        createRandom()
    }

    private fun createRandom() {
        Handler().postDelayed(runTimeRandom,2000)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
        //test

        createRandom()

        ObservingService.addObserver("BLE_DATA", observerss)

    }

    private fun checkInternetAndLocation() {

        if(activity.isNetworkConnected()) {
            /// Get location data
            try {
                val locationReq = LocationRequest()

            } catch (e:Exception){}
        }

    }

    override fun onStop() {
        super.onStop()
        ObservingService.removeObserver("BLE_DATA",observerss)
    }

    private val observerss = Observer { observable, any ->

        val randd = Random()
        val doubl = randd.nextInt(60) + 60
        pbHeartRate.progress = doubl

        tvHR.text = "$doubl"
    }

}