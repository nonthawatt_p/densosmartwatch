package com.ssd.densosmartwatch

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentTransaction
import android.widget.Toast
import com.google.android.gms.common.api.GoogleApiClient
import com.ssd.densosmartwatch.utilities.KUtil

class SplashScreen : KActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


//    private val REQUEST_ENABLE_BLUETOOTH = 1
//    private val REQUEST_ENABLE_LOCATION = 2
//    private val REQUEST_COARSE_LOCATION = 1

    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null

//    private var mGoogleApiClient: GoogleApiClient? = null
//    private var mLocationRequest: LocationRequest? = null
//    private var mLocationResult: PendingResult<LocationSettingsResult>? = null

    private var mTransaction: FragmentTransaction? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        Handler().postDelayed({
            if(KUtil.debug) {
                KUtil.setMacAddress(this@SplashScreen,"ac:bc:32:f1:53:d9")
                gotoNext()
                return@postDelayed
            }
            checkPermission()
        }, 1000)

    }

    private fun checkPermission() {

        // Check if BLE is supported on the device.
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Bluetooth LE not supported in this device!", Toast.LENGTH_SHORT).show()
            finish()
        }

        mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = mBluetoothManager!!.adapter

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth Adapter not supported in this device!", Toast.LENGTH_SHORT).show()
            finish()

            return
        }

        if (mBluetoothAdapter != null) {
            // Is Bluetooth turned on?
            if (mBluetoothAdapter!!.isEnabled) {
                checkLocation({
                    gotoNext()
                })
            } else {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH)
            }
        }

    }

    /*private fun checkLocation() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        mGoogleApiClient!!.connect()

        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Location Permission")
                builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.")
                builder.setPositiveButton(android.R.string.yes) { dialogInterface, i ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_COARSE_LOCATION)
                    }
                }
                builder.setNegativeButton(android.R.string.no, null)

                builder.show()
            } else {
                gotoNext()
            }
        } else {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGpsProviderEnabled: Boolean = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkProviderEnabled: Boolean = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)


            if (!isGpsProviderEnabled && !isNetworkProviderEnabled) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Location Permission")
                builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.")
                builder.setPositiveButton(android.R.string.yes) { dialogInterface, i ->
                    // enable location

                }
                builder.setNegativeButton(android.R.string.no, null)

                builder.show()
            } else {
                gotoNext()
            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        ///
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = create()
        mLocationRequest!!.priority = PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.interval = (30 * 1000).toLong()
        mLocationRequest!!.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)

        mLocationResult = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        mLocationResult!!.setResultCallback({ result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();

            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> { }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@SplashScreen, REQUEST_ENABLE_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> { }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        })
    }

    override fun onConnectionSuspended(p0: Int) {
        /// Suspend
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        when (requestCode) {
            REQUEST_ENABLE_BLUETOOTH -> when (resultCode) {
                Activity.RESULT_OK -> {
                    checkLocation({
                        gotoNext()
                    })
                }
                Activity.RESULT_CANCELED -> {
                    gotoNext()
                    return
                }
                else -> {
                }
            }
            REQUEST_ENABLE_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                    // All required changes were successfully made
//                    Toast.makeText(this@SplashScreen, "Location enabled", Toast.LENGTH_LONG).show()
                    gotoNext()
                }
                Activity.RESULT_CANCELED -> {
                    // The user was asked to change settings, but chose not to
//                    Toast.makeText(this@SplashScreen, "Location not enabled", Toast.LENGTH_LONG).show()
                    gotoNext()
                }
                else -> {
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_COARSE_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    println("Coarse Location Permission granted")
                    gotoNext()
                } else {
//                    finish()
                    gotoNext()
                    return
                }
                return
            }
        }
    }

    private fun gotoNext() {
        val mcadr = KUtil.getMacAddress(this)
        val intent = if (mcadr != null) {
            if(KUtil.getToken(this) != null) {
                KUtil.acToken = KUtil.getToken(this)
                Intent(this@SplashScreen, HomeActivity::class.java)//LoginActivity::class.java)
            } else {
                Intent(this@SplashScreen, LoginActivity::class.java)//LoginActivity::class.java)
            }

        } else {
            Intent(this@SplashScreen, WearableActivity::class.java)
        }
        intent.putExtra("fromSplash",true)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        overridePendingTransition(R.anim.fadein,R.anim.fadeout)
        finish()


    }
}
