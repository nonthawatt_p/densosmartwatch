package kilmuz.kotlinlib.lib

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.support.annotation.StringRes
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.ssd.densosmartwatch.R

/**
 * Created by Nonth on 9/5/2017 AD.
 */

class KAlert {

    companion object {

        var onOneClickListener: ((dialogInterface: DialogInterface) -> Unit)? = null

        private var alerts:AlertDialog.Builder? = null

        private fun onOneListener(dialogInterface: DialogInterface) {
            onOneClickListener?.invoke(dialogInterface)
        }

    @JvmStatic fun initial(activity: Activity) : KAlert.Companion {
            alerts =  AlertDialog.Builder(activity)
            return KAlert.Companion
        }

    @JvmStatic fun showCustom(activity: Activity, title: String, message: String, btnTitle: String, listener: ((View) -> Unit)?) {

            val builder = AlertDialog.Builder(activity)
            val view = activity.layoutInflater.inflate(R.layout.alert_dialog, null)
            builder.setView(view)
            val dialog = builder.create()
            //        dialog.setContentView(R.layout.alert_dialog);
            //        dialog.setTitle(title);

            val tvTitle = view.findViewById<TextView>(R.id.alert_dialog_title)
            tvTitle.text = title
            val tvMsg = view.findViewById<TextView>(R.id.alert_dialog_msg)
            tvMsg.text = message
            val btnDialog = view.findViewById<Button>(R.id.alert_dialog_btn_ok)
            btnDialog.text = btnTitle
            btnDialog.setOnClickListener { view ->
                listener?.invoke(view)
                dialog.dismiss()
            }
            dialog.setCancelable(false)
            dialog.show()
        }

        @JvmStatic fun showCustomEdt(activity: Activity, listener: ((EditText,AlertDialog) -> Unit)?) {

            val builder = AlertDialog.Builder(activity)
            val view = activity.layoutInflater.inflate(R.layout.alert_dialog_with_edt, null)
            builder.setView(view)
            val dialog = builder.create()

            val edtPassw = view.findViewById<EditText>(R.id.alert_dialog_edt_passw)

            val btnDialog = view.findViewById<Button>(R.id.alert_dialog_btn_ok)
            btnDialog.setOnClickListener { _ ->
                listener?.invoke(edtPassw,dialog)
            }
            val btnCancel = view.findViewById<Button>(R.id.alert_dialog_btn_cancel)
            btnCancel.setOnClickListener { _ ->
                dialog.dismiss()
                activity.hideKeyboard()
            }
            dialog.setCancelable(false)
            dialog.show()
        }

    @JvmStatic fun showWithOKAndCancelButton(activity: Activity, @StringRes title: Int, @StringRes message: Int, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
            show(activity, activity.resources.getString(title), activity.resources.getString(message), "Cancel", "OK", listener)
        }
    @JvmStatic fun showWithOKAndCancelButton(activity: Activity, @StringRes title: Int, message: String, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
            show(activity, activity.resources.getString(title), message, "Cancel", "OK", listener)
        }
    @JvmStatic fun showWithOKAndCancelButton(activity: Activity, title: String, @StringRes message: Int, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
            show(activity, title, activity.resources.getString(message), "Cancel", "OK", listener)
        }
    @JvmStatic fun showWithOKAndCancelButton(activity: Activity, title: String, message: String, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
            show(activity, title, message, "Cancel", "OK", listener)
        }

    @JvmStatic fun showWithTwoBtnTitle(activity: Activity, @StringRes title: Int, @StringRes message: Int, cancel: String, ok: String, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
            show(activity, activity.resources.getString(title), activity.resources.getString(message), cancel, ok, listener)
        }

    @JvmStatic fun show(activity: Activity, title: String, message: String, btnCancelTitle: String, btnOKTitle: String, listener: (_OnClickTwoButtonListener.() -> Unit)?) {
        val callback = _OnClickTwoButtonListener()
        if (listener != null) {
            callback.listener()
        }
            createAlert(activity, title, message)
                    .setNegativeButton(btnCancelTitle) { dialogInterface, i ->
                        callback.onCancel(dialogInterface)
                    }
                    .setPositiveButton(btnOKTitle) { dialogInterface, i ->
                        callback.onOK(dialogInterface)
                    }.show()
        }


    @JvmStatic fun showWithOK(activity: Activity, @StringRes title: Int, message: String, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, activity.resources.getString(title), message, "OK", listener)
        }

    @JvmStatic fun showWithOK(activity: Activity, title: String, @StringRes message: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, title, activity.resources.getString(message), "OK", listener)
        }

    @JvmStatic fun showWithOK(activity: Activity, title: String, message: String, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, title, message, "OK", listener)
        }

    @JvmStatic fun showWithOK(activity: Activity, @StringRes title: Int, @StringRes message: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, activity.resources.getString(title), activity.resources.getString(message),"OK", listener)
        }


    @JvmStatic fun show(activity: Activity, @StringRes title: Int, message: String, @StringRes buttonTitle: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, activity.resources.getString(title), message, activity.resources.getString(buttonTitle), listener)
        }

    @JvmStatic fun show(activity: Activity, title: String, @StringRes message: Int, @StringRes buttonTitle: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, title, activity.resources.getString(message), activity.resources.getString(buttonTitle), listener)
        }

    @JvmStatic fun show(activity: Activity, title: String, message: String, @StringRes buttonTitle: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, title, message, activity.resources.getString(buttonTitle), listener)
        }

    @JvmStatic fun show(activity: Activity, @StringRes title: Int, @StringRes message: Int, @StringRes buttonTitle: Int, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, activity.resources.getString(title), activity.resources.getString(message), activity.resources.getString(buttonTitle), listener)
        }

        @JvmStatic fun show(activity: Activity, title: String, message: String, listener: ((dialogInterface: DialogInterface) -> Unit)?) {
            show(activity, title, message, activity.getString(R.string.ok), listener)
        }

    @JvmStatic fun show(activity: Activity, title: String, message: String, buttonTitle: String, listener: ((dialogInterface: DialogInterface) -> Unit)?) {

            createAlert(activity, title, message)
                    .setPositiveButton(buttonTitle) { dialogInterface, i ->
                        listener?.invoke(dialogInterface)
                    }.show()
        }

        private fun createAlert(activity: Activity, title: String, message: String): AlertDialog.Builder {
            return AlertDialog.Builder(activity)
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(false)
        }

        @JvmStatic fun setText(title: String, message: String): AlertDialog.Builder {
            alerts?.setTitle(title)?.setMessage(message)
            return alerts!!
        }


    }

    interface OnClickTwoButtonListener {
        fun onCancel(dialogInterface: DialogInterface)
        fun onOK(dialogInterface: DialogInterface)
    }

    interface OnClickListener {
        fun onClick(dialogInterface: DialogInterface)
    }

}

class _OnClickListener : KAlert.OnClickListener {
    private var _onClick: ((dialogInterface:DialogInterface) -> Unit)? = null

    override fun onClick(dialogInterface: DialogInterface) {
        _onClick?.invoke(dialogInterface)
    }

    fun onClick(func: (dialogInterface:DialogInterface) -> Unit) {
        _onClick = func
    }
}

class _OnClickTwoButtonListener : KAlert.OnClickTwoButtonListener {


    private var _onOK : ((dialogInterface:DialogInterface) -> Unit)? = null
    private var _onCancel : ((dialogInterface:DialogInterface) -> Unit)? = null

    override fun onOK(dialogInterface: DialogInterface) {
        _onOK?.invoke(dialogInterface)
    }

    override fun onCancel(dialogInterface: DialogInterface) {
        _onCancel?.invoke(dialogInterface)
    }

    fun onOK(func: (dialogInterface:DialogInterface) -> Unit) {
        _onOK = func
    }
    fun onCancel(func: (dialogInterface:DialogInterface) -> Unit) {
        _onCancel = func
    }
}

//inline fun KAlert.Companion.show(activity: Activity, title: String, message: String, btnCancelTitle: String, btnOKTitle: String, func: _OnClickTwoButtonListener.() -> Unit) {
//    val callback = _OnClickTwoButtonListener()
//    callback.func()
//    KAlert.show(activity,title,message,btnCancelTitle,btnOKTitle,callback)
//}

//typealias kAlertListener = (dialog:DialogInterface) -> Unit?